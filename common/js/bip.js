var bip = {

  deviceType: 0,
  callbacks: {},
  init: function() {
    var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);
    var Android = (navigator.userAgent.match(/(Android)/g) ? true : false);

    if (iOS) this.deviceType = 1;
    if (Android) this.deviceType = 2;

  },
  callOnResume: function() {
    if (this.onResume) {
      this.onResume();
    }
  },
  send: function(opts) {

    console.log('Bip send message called with data: ' + JSON.stringify(opts));

    if (opts.callback) {
      var callbackId = Math.random() + '';
      this.callbacks[callbackId] = opts.callback;
      opts.callbackId = callbackId;
    }

    if (this.deviceType == 0) {
      //window.open(opts.url);
    } else if (this.deviceType == 1) {
      sendJsonToiOS(opts);
    } else if (this.deviceType == 2) {
      BIPAndroidJSObject.onHtmlCommand(JSON.stringify(opts));
    }


  },
  onCallback: function(callbackId, data) {
    this.callbacks[callbackId](unescape(data));
  }

}

function execute(url) {
  var iframe = document.createElement("IFRAME");
  iframe.setAttribute("src", url);
  document.documentElement.appendChild(iframe);
  iframe.parentNode.removeChild(iframe);
  iframe = null;
}

function sendJsonToiOS(params) {
  var JSONString = (JSON.stringify(params));
  var URLString = encodeURI(JSONString);
  var uri = 'bipjs://' + '' + "" + URLString;
  var iframe = document.createElement("iframe");
  iframe.src = uri;
  document.body.appendChild(iframe);
  document.body.removeChild(iframe);
}

bip.init();
